<?php
class mod_blog extends ci_model{

	function select_all(){
		return $this->db->get('artikel');
	}


	function select_one($id){
		return $this->db->get_where('artikel',array('artikel_id'=>$id));
	}

	function simpan_komentar(){
		$author=$this->input->post('author');
		$komentar=$this->input->post('komentar');
		$artikel_id=$this->input->post('id');
		$data=array('author'=>$author,'isi_komentar'=>$komentar,'artikel_id'=>$artikel_id);
		$this->db->insert('komentar',$data);
	}

	function tampil_komentar($artikel_id){
		return $this->db->get_where('komentar',array('artikel_id'=>$artikel_id));
	}
}