<?php
class Blog extends ci_controller{

	function __construct(){
		parent::__construct();
		$this->load->model('mod_blog');
	}

	function index(){
		$data['title']="Daftar Artikel";
		// load model blog
		$data['list_artikel']=$this->mod_blog->select_all();
		$this->load->view('list_blog',$data);

	}

	function read(){
		$id=$this->uri->segment(3);
		$data['artikel']=$this->mod_blog->select_one($id)->row_array();
		$data['komentar']=$this->mod_blog->tampil_komentar($id)->result();
		$this->load->view('detail',$data);
	}

	function simpan_komentar(){
		$this->mod_blog->simpan_komentar();
		redirect('blog/read/'.$_POST['id']);
	}

	function hapus_komentar(){
		//  id komentar
		$id=$this->uri->segment(3);
		// artikel_id untuk redirect
		$data=$this->db->get_where('komentar',array('komentar_id'=>$id))->row_array();
		$id_artikel=$data['artikel_id'];
		// delete komentar
		$this->db->where('komentar_id',$this->uri->segment(3));
		$this->db->delete('komentar');
		// redirect ke read
		redirect('blog/read/'.$id_artikel);
	}
}


?>